# Test project: KR6947-svedova

## Steps

1. Create blank repository with SAST configuration enabled

## Considerations and Questions

1. Where does the following come from?

```yml
include:
- template: Security/SAST.gitlab-ci.yml
```

1. Despite having SAST enabled in `.gitlab-ci.yml` file, I still see it as not enabled on Security Configuration:

![sast-not-enabled](assets/sast-not-enabled.png)

## First try

1. Enabled Secret Detection with a merge request
1. Created [main.go](./src/main.go) file with two constants in it
1. Ran a pipeline
1. First observation: https://gitlab.com/gitlab-org/govern/threat-insights-demos/verification-projects/kr6947-svedova/-/merge_requests/2#note_1940722140
1. Managed to create a vulnerability through secret detection. https://gitlab.com/gitlab-org/govern/threat-insights-demos/verification-projects/kr6947-svedova/-/merge_requests/2 contains more information on this

## Sast vulnerability

1. Here's the outcome of the MR which produced a SAST vulnerability: https://gitlab.com/gitlab-org/govern/threat-insights-demos/verification-projects/kr6947-svedova/-/merge_requests/4#note_1943144048

## Dependency Scanner

1. Enabled the scanner through an MR: https://gitlab.com/gitlab-org/govern/threat-insights-demos/verification-projects/kr6947-svedova/-/merge_requests/
