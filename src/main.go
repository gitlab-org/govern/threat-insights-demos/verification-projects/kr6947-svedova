package main 

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

const DB_USERNAME= "lorem-ipsum-dolor"
const DB_PASSWORD= "sit-amet"
const EXPOSED_PAT= 'glpat-GahyuqrbtKkrZQjYBAeu'

func main() {
	http.HandleFunc("/user", getUser)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func getUser(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("username")

	if username == "" {
		http.Error(w, "Missing username", http.StatusBadRequest)
		return
	}

	// Vulnerable to SQL injection
	query := fmt.Sprintf("SELECT * FROM users WHERE username='%s'", username)
	
	user, err := fetchUser(query)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "User: %+v", user)
}

func fetchUser(username string) (map[string]string, error) {
	db, err := sql.Open("mysql", "user:password@/dbname")

	if err != nil {
		return nil, err
	}
	
	defer db.Close()

	query := "SELECT * FROM users WHERE username = ?"
	rows, err := db.Query(query, username)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Assuming only one user is fetched
	user := make(map[string]string)
	if rows.Next() {
		var id, username, email string
		if err := rows.Scan(&id, &username, &email); err != nil {
			return nil, err
		}
		user["id"] = id
		user["username"] = username
		user["email"] = email
	}

	return user, nil
}
